from email import policy
from email.parser import BytesParser
import re
from pathlib import Path
from collections import defaultdict
import csv
import argparse


email_regex = r"([^<]*)<([^>]+)>"
mail_to_names = defaultdict(list)


def visit_eml_file(eml_file):
    with open(eml_file, 'rb') as fp:
        msg = BytesParser(policy=policy.default).parse(fp)
    from_str = msg['to']
    if from_str is not None:
        tos = re.findall(email_regex, from_str)
        if len(tos) > 0:
            name, email = tos[0]
            mail_to_names[email].append(name)


def visit_files(directory):
    directory_path = Path(directory)
    for file_path in directory_path.rglob('*'):
        if file_path.is_file() and str(file_path).endswith(".eml"):
            visit_eml_file(file_path)


parser = argparse.ArgumentParser("mail browser")
parser.add_argument("input-dir", dest="input_dir",
                    help="the absolute path to the directory containing eml files in a folder hierarchy")
parser.add_argument("output-csv", dest="output_csv",
                    help="the absolute path to the CSV file wo write")

args = parser.parse_args()
root = args.input_dir
visit_files(root)

mail_to_names_unique = {}
keys = []
for key in mail_to_names.keys():
    keys.append(key)
    mail_to_names_unique[key] = set(mail_to_names[key])
keys.sort()


with open(args.output_csv, 'w', encoding="utf-8") as f:
    writer = csv.writer(f)
    for key in keys:
        try:
            writer.writerow([key, ','.join(mail_to_names_unique[key]).replace("\"", "").strip()])
        except Exception as err:
            print(f"{type(err).__name__} was raised: {err}")
