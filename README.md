# Mail browser

## Purpose
extract sender addresses from mails exported from MailStore Home

## Motivation
In Thunderbird there is no way to fill up the address book with the email 
addresses from all received emails in your post box.

## How-to
Use [MailStore Home](https://www.mailstore.com/de/produkte/mailstore-home/) 
to archive your email post box. Then export those emails to a local directory
as eml files as described [here](https://help.mailstore.com/de/home/E-Mails_exportieren).
With `python main <absolute path to the directory containing the just exported mails> <absolute path to CSV file to write>`
the distinct email addresses in the first column and the names associated with 
them as comma separated list in the second column are exported.
